print("=Love2D API Builder=")
print("By sheepmanbuddy :D")

assert(_VERSION:find("5"), "Use Lua 5.1 for this script (5.2+ may not work)")

local api = require("love_api")

local args = {...}


local noDescrip = false
for key, arg in ipairs(args) do
	if arg == "--nodescrip" then
		print("Disabling discriptions...")
		noDescrip = true
	end
end

local file = io.open("lua.xml", "w")
local flist = io.open("flist.txt", "w")

local cmod = nil

function formatModule(funcname)
	local modulename = "love"
	
	if cmod then
		modulename = modulename .. "." .. cmod
	end
	
	if type(funcname) == "string" and funcname ~= "" then
		modulename = modulename .. "." .. funcname
	end
	
	flist:write(modulename .. "\n")
	return modulename
end

function writeFunction(funcdata)
	local name = funcdata.name
	local descrip = funcdata.description or ""
	descrip = clean(descrip)
	if noDescrip then
		descrip = ""
	end
	
	file:write([[
	<KeyWord name="]] .. formatModule(name) .. [[" func="yes">
]])

	if funcdata.variants then
		for key, variant in ipairs(funcdata.variants) do
			writeVariant(variant, descrip)
		end
	end
	
	file:write([[
	</KeyWord>
	
]])
end

function writeVariant(variant, descrip)
	local retVal = ""
	for key, ret in ipairs(variant.returns or {}) do
		retVal = retVal .. ret.type .. " " .. ret.name
		if key ~= #variant.returns then
			retVal = retVal .. ", "
		end
	end
	
	if noDescrip then
		descrip = ""
	end
	
	if not variant.arguments then
		file:write([[
		<Overload retVal="]] .. retVal .. [[" descr="]] .. clean(descrip) .. [["/>
]])
	else
		file:write([[
		<Overload retVal="]] .. retVal .. [[" descr="]] .. clean(descrip) .. [[">
]])
		
		for key, param in ipairs(variant.arguments) do
			writeParam(param)
		end
		
		file:write([[
</Overload>
]])
	end
end

function writeParam(param)
	file:write([[
	<Param name="]] .. param.type .. " " .. clean(param.name) .. [["/>
]])
end

function clean(text)
	text = text:gsub("\"", "'")
	return text
end

file:write([[
<?xml version="1.0" encoding="Windows-1252" ?>
<NotepadPlus>
<AutoComplete language="Lua">
<Environment ignoreCase="yes" startFunc="(" stopFunc=")" paramSeparator="," terminal=";" additionalWordChar="."/>
	
<KeyWord name="]] .. formatModule() .. [["/>
]])

-- include the functions --
for key, funcdata in ipairs(api.functions) do
	print(key, funcdata)
	writeFunction(funcdata)
end

for key, funcdata in ipairs(api.callbacks) do
	writeFunction(funcdata)
end

for key, mod in ipairs(api.modules) do
	cmod = nil
	local name = mod.name
	
	file:write([[
	<KeyWord name="]] .. formatModule(name) .. [["/>
]]
	)
	
	cmod = name
	
	for key2, funcdata in ipairs(mod.functions) do
		writeFunction(funcdata)
	end
end
	

file:write([[
</AutoComplete>
</NotepadPlus>
]])

file:close()

print("API Built!")