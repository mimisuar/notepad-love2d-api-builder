# Notepad++ Love2D API Builder

Builds the Love2D api for notepad++ from a lua script.

## How to use 

First, get a copy of a the love2d lua script from here: https://github.com/love2d-community/love-api

Next, make sure **build_api.lua** is in the same folder as **modules** and **love_api.lua**. 

Finally, run **build_api.lua** using *Lua 5.1* (5.2+ higher may not work).  
**flist.txt** contains all of the functions and tables that were added for debug purposes. This file can be safely ignored or removed.  
**lua.xml** contains the actual api and should be placed in notepad++'s api folder. 